
import React from 'react';
import './App.css';

// function App() {
//   const num = 3;
//   const hour = 'hour';
//   const isPM = true;
//   const nullish = null;
//   return (
//     <div>
//       <ul>
//         <li>{num}</li>
//         <li>{hour}</li>
//         <li>{isPM.toString()}</li>
//         <li>{nullish}</li>
//       </ul>
//     </div>
//   );
// }

// export default App;

const data = [
  {
    name: 'Ivan',
    age: '22',
  },
  {
    name: 'Petr',
    age: '21',
  },
  {
    name: 'Hussein',
    age: '31',
  },
  {
    name: 'Pafnutiy',
    age: '44',
  }
]

class App extends React.Component {
  constructor(props) {
    super(props);
    this.num = 3;
    this.hour = 'hour';
    this.isPM = true;
    this.nullish = null;
    this.state = {
      windowSize: [window.innerWidth, window.innerHeight],
    };
  };

  handleWindowResize = () => {
    this.setState({
      windowSize: [window.innerWidth, window.innerHeight],
    });
  };

  componentDidMount() {
    console.log('Component has been mounted');
    window.addEventListener('resize', this.handleWindowResize);
  }

  componentWillUnmount() {
    console.log('Component has been unmounted');
    window.removeEventListener('resize', this.handleWindowResize);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.age !== this.props.age) {
      console.log('Change state or fetch new data');
    }
  }

  render() {
    return (
     <div className='users'>
        { data.map((user) => 
        <ul>
             <li>{ user.name }</li>
             <li>{ user.age }</li>
             <button>+</button>
             <button>-</button>
        </ul>
        )}
    </div>
    )
  }
}

export default App
